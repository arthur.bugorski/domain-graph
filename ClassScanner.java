package com.renorun.domain;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;

import javax.persistence.Entity;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Optional;

// 1. add to your local build.gradle
//            implementation 'io.github.classgraph:classgraph:4.8.146'
// 2. copy this class into your project and run it
// 3. make sure you have Graphviz installed in you classpath
// 4. run: neato -Tsvg -o"domain.svg" .\domain.dot"
public class ClassScanner {

    private static final Class< ? extends Annotation > desiredClassAnnotation = Entity.class;

    public static void main( String[] args ) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException{
        String pkg = "com.renorun";//.domain";
        String desiredClassAnnotationName = desiredClassAnnotation.getName();

        final StringBuilder bodyBuilder = new StringBuilder();
        try ( ScanResult scanResult =
                      new ClassGraph()
//                             .verbose()               // Log to stderr
                              .enableAllInfo()         // Scan classes, methods, fields, annotations
                              .acceptPackages(pkg)     // Scan com.xyz and subpackages (omit to scan all packages)
                              .scan()
        ) {               // Start the scan
            for ( ClassInfo routeClassInfo : scanResult.getClassesWithAnnotation(desiredClassAnnotationName)) {
                final Class<?> entityClass = Class.forName( routeClassInfo.getName() );

                for( Field field : entityClass.getDeclaredFields() ){
                    final Optional< Class< ? > > entityReference = getReferencedEntity( field );
                    if( entityReference.isPresent() ){
                        final Class< ? > referencedEntity = entityReference.get();

                        final String referrerName = entityClass.getSimpleName();
                        final String fieldName = field.getName();
                        final String referrantName = referencedEntity.getSimpleName();

                        bodyBuilder.append(
                                String.format(
                                        "\t%1$s -> %3$s [ label=\"%2$s\", color=\"%4$s\", len=\"8\" ];\n",
                                        referrerName,
                                        fieldName,
                                        referrantName,
                                        randomColour()
                                )
                        );
                    }
                }
            }
        }
        final String prologue = "digraph G {\n";
        final String body = bodyBuilder.toString();
        final String epilogue = "}";


        final File f = new File( "c:/Users/Art/Documents/domain.dot" );
        f.delete();
        try( FileWriter writer = new FileWriter( f ); BufferedWriter bufOut = new BufferedWriter( writer )  ){
            bufOut.write( prologue );
            bufOut.write( body );
            bufOut.write( epilogue );

        }catch( Exception e ){
            e.printStackTrace();

        }
    }

    /**
     * {@code Optional#empty}
     */
    private static Optional<Class<?>> getReferencedEntity( final Field f ){
        final Class< ? > fieldClass = f.getType();

        if( hasDesiredAnnotation( fieldClass ) ){
            return Optional.of( fieldClass );

        }else if( Collection.class.isAssignableFrom(fieldClass) ){
            final Type genericType = f.getGenericType();

            try{
                //this is to use a com.sun method because due to erasure your not supposed to know about type arguments
                final Method m = genericType.getClass().getMethod( "getActualTypeArguments" );
                final Type[] typeArguments = (Type[]) m.invoke( genericType );

                final Class<?> typeClass = (Class<?>) typeArguments[0];

                if( hasDesiredAnnotation( typeClass ) ){
                    return Optional.of( typeClass );
                }else{
                    return Optional.empty();
                }

            }catch( NoSuchMethodException | IllegalAccessException | InvocationTargetException e ){
                e.printStackTrace();
                return Optional.empty();
            }

        }else{
            return Optional.empty();

        }
    }

    private static boolean hasDesiredAnnotation( final Class<?> clazz ){
        return clazz.isAnnotationPresent( desiredClassAnnotation );
    }

    private static String randomColour(){
        return DOT_COLOURS[ (int) (Math.random() * DOT_COLOURS.length) ];
    }

    private static final String[] DOT_COLOURS = {
            "aliceblue", "goldenrod", "aqua", "aquamarine", "azure", "beige", "bisque", "black",
            "blue", "blueviolet", "brown", "burlywood", "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue",
            "cornsilk", "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkgrey",
            "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen",
            "darkslateblue", "darkslategray", "darkslategrey", "darkturquoise", "darkviolet", "deeppink", "deepskyblue",
            "dimgray", "dodgerblue", "firebrick", "forestgreen", "fuchsia", "gainsboro", "gold", "gray",
    };
}
